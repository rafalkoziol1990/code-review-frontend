import { ProjectFlowMk2Page } from './app.po';

describe('project-flow-mk2 App', () => {
  let page: ProjectFlowMk2Page;

  beforeEach(() => {
    page = new ProjectFlowMk2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
