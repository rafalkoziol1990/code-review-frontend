import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserLoginComponent } from './users/user-login/user-login.component';
import { UserRegistrationComponent } from './users/user-registration/user-registration.component';

import { Router, RouterModule, PreloadAllModules } from '@angular/router';

import { IndexComponent } from './index/index.component';

import { AuthGuard } from './users/auth.guard';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot([
      { path: '', component: IndexComponent },
      { path: 'login', redirectTo: '/user/login' },
      { path: 'registry', redirectTo: '/user/registry' },
      { path: 'restore-password', redirectTo: '/user/restore-password' },
      { path: 'project', loadChildren: 'app/project/project.module#ProjectModule', canLoad: [AuthGuard] },
      // { path: '**', redirectTo: '/error/404' }
    ], { preloadingStrategy: PreloadAllModules, enableTracing: false })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
