import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from './users/users.service';
import { NavService } from './shared/nav/nav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public isAuth: boolean;
  public appHeaderConfig = {
    displayHeader: true,
    style: {
      'background-color': 'rgba(66, 133, 244, 1)',
      'color': '#fff'
    }
  }

  constructor(private usersService: UsersService, private navService: NavService) {
    // usersService.updateUserData();
    usersService.isAuthenticate().subscribe(
      (res) => {
        console.log('isAuthenticate? :', res.status)
        this.isAuth = res.status;
        if (res.status) {
          this.usersService.setAuthStatus(true);
        } else {
          this.usersService.setAuthStatus(false);
        }
      });

    usersService.isAuth$.subscribe(
      (res) => {
        this.isAuth = res;
      })

    navService.nav$.subscribe((res) => {
      console.log(res)
      this.isAuth = res.isAuth;
      this.appHeaderConfig.displayHeader = res.mainHeader.visible;
      this.closeSidenav();
    });
  }

  SignOut() {
    this.usersService.logout();
  }

  @ViewChild('sidenav') sidenav;

  openSidenav() {
    this.sidenav.open();
  }
  closeSidenav() {
    this.sidenav.close();
  }

  ngOnInit() {
    // setInterval(() => {
    // console.log(this.sidenav.toggle());

    // this.appHeaderConfig.displayHeader = !this.appHeaderConfig.displayHeader;
    // this.usersService.setAuthStatus(!this.isAuth);
    // }, 4000)
  }
}
