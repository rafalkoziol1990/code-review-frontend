import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FileModule } from './../shared/file/file.module';

import { HttpModule } from '@angular/http';

import { ProjectComponent } from './project.component';
import { ProjectService } from './project.service';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectChatComponent } from './project-edit/project-chat/project-chat.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectTicketsComponent } from './project-edit/project-tickets/project-tickets.component';
import { ProjectContractorsComponent } from './project-edit/project-contractors/project-contractors.component';
import { ProjectRoutingModule } from './project-routing.module';
import { MaterialModule } from '@angular/material';

import { HeaderModule } from './../shared/header/header.module';

import { MyDynamincFormModule } from './../shared/my-form/my-dynamic-form.module';

@NgModule({
  imports: [
    CommonModule,
    FileModule,
    FormsModule,
    HeaderModule,
    HttpModule,
    MaterialModule,
    MyDynamincFormModule,
    ProjectRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ProjectChatComponent,
    ProjectComponent,
    ProjectContractorsComponent,
    ProjectEditComponent,
    ProjectListComponent,
    ProjectTicketsComponent,
  ],
  providers: [ProjectService]
})
export class ProjectModule { }
