import { Injectable } from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';

// Statics
import 'rxjs/add/observable/throw';

// Operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

import { Observable, Subject } from 'rxjs/Rx';

export { Observable, Subject } from 'rxjs/Rx';

import { Project } from './project'
import { ApiService } from './../shared/api.service';

@Injectable()
export class ProjectService extends ApiService<Project> {
  private servicePostfix = 'project';

  private projects = new Subject<Project[]>();
  projects$ = this.projects.asObservable();

  constructor(http: Http) {
    super(http);
    this.setModuleName(this.servicePostfix);
  }

  setProjects(projects: Project[]): void {
    this.projects.next(projects);
  }

  getProject(id: string): Promise<Project> {
    return new Promise((resolve, reject) => {
      this.fetchRow(id).subscribe((res) => {
        resolve(res);
      })
    })
  }
}
