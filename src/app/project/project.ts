export class Project {
    public _id: string;
    public user: any;
    public title: string;
    public description: string;
    public finish: any;
    public active: boolean;
    public tickets: [{
        content: string,
        finished: boolean,
        createdAt: any
    }];
    public messages: [{
        user: any,
        data: string,
        added: any,
    }];
    public contractors: [{
        user: any
    }];
    public tmp: any;

    constructor(params: {
        _id?: string,
        user?: any,
        title?: string,
        description?: string,
        finish?: Date,
        active?: boolean,
        tickets?: [{
            content: string,
            finished: boolean,
            createdAt: Date
        }],
        messages?: [{
            user: any,
            data: string,
            added: Date,
        }],
        contractors?: [{
            user: any
        }],
        tmp?: any
    }) {
        this._id = params._id || '';
        this.user = params.user || {};
        this.title = params.title || '';
        this.description = params.description || '';

        this.finish = params.finish || new Date();
        // this.active = params.active || true;
        this.active = !!params.active;
        this.tickets = params.tickets || [{ content: '', finished: false, createdAt: new Date() }];
        this.messages = params.messages || [{ user: {}, data: '', added: new Date() }];
        this.contractors = params.contractors || [{ user: '' }];
        this.tmp = params.tmp || {};
    };
}

