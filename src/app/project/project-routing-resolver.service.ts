import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';

import { Project } from './project';
import { ProjectService } from './project.service';

@Injectable()
export class ProjectRoutingResolver implements Resolve<Project> {
  constructor(private projectService: ProjectService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Project> {
    const id = route.paramMap.get('id');

    return this.projectService.getProject(id)
      .then((res) => {
        if (res) {
          return res;
        } else { // id not found
          this.router.navigate(['/project']);
          return null;
        }
      })
  }
}
