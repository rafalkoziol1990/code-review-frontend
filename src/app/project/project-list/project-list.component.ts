import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { ProjectService } from './../project.service';
import { ApiQueryModel } from './../../shared/api-query-model';
import { Project } from './../project';

import { FileService } from './../../shared/file/file.service';

import { HeaderModule } from './../../shared/header/header.module';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css'],
  animations: [
    trigger('flyInOut', [
      // state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({ transform: 'translateY(-100%)' }),
        animate(400)
      ]),
      transition('* => void', [
        animate(400, style({ transform: 'translateY(100%)' }))
      ])
    ])
  ]
})
export class ProjectListComponent implements OnInit {
  public fabBtnPulse = true;
  public searchInput = new Subject();
  public projectsFromStream: Observable<Project[]>;
  public projects: Project[];
  public query = new ApiQueryModel({
    pagination: { limit: 16, page: 0, total: null },
    query: { active: true, deleted: false },
    select: 'title description createdAt updatedAt user'
  })
  public appHeaderConfig = {
    displayHeader: true,
    style: {
      'background-color': 'rgba(255,255,255,1)',
      'color': 'rgba(0,0,0,0.87)'
    }
  };
  public fabOpen = false;
  public apiUrl = '';

  constructor(
    private projectService: ProjectService,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private fileService: FileService,
  ) {
    this.projectsFromStream = this.projectService.projects$;

    this.activatedRoute.params.subscribe((params: any) => {
      if (params.hasOwnProperty('mode')) {
        if (params['mode'] === 'archive') {
          this.query.query.active = false;
        } else {
          this.query.query.active = true;
        }
      }
      if (params.hasOwnProperty('query')) {
        this.query.query.title = params['query'];
      }
      if (params.hasOwnProperty('page')) {
        this.query.pagination.page = params['page'];
      }
      this.getProjects();
    });
    this.apiUrl = fileService.getApiUrl();
  }

  getProjects() {
    this.projectService.fetchAll(this.query)
      .subscribe(
      (res) => {
        // update project stream
        this.projectService.setProjects(res.data);
        this.projects = res.data;
        this.query = res.query;
        this.projects.map((v, i) => {
          const filesQuery = new ApiQueryModel({
            pagination: { limit: 1, page: 0, total: null },
            query: { deleted: false, parentid: v._id, keyword: 'project' },
            select: 'filename originalname destination createdAt updatedAt'
          });
          this.fileService.fetchAll(filesQuery)
            .subscribe((fileRes) => {
              const userExtra = { sign: 'X', avatar: '' };
              if (v.user) {
                userExtra.sign = v.user.login.charAt(0).toUpperCase();
                userExtra.avatar = this.apiUrl + 'public/uploads/' + v.user._id + '/userAvatar/' + v.user._id + '/s/' + v.user.avatar;
              }
              this.projects[i].tmp = { files: fileRes.data, userExtra: userExtra };
              // update project stream
              this.projectService.setProjects(res.data);
            })
        })
      });
  }

  pageEvent(e) {
    this.query.pagination.page = e.pageIndex;
    this.query.pagination.limit = e.pageSize;
    this.setState();
    this.getProjects();
  }

  searchInputChange(searchInput) {
    this.searchInput.next(searchInput);
  }

  openFab() {
    this.fabOpen = true;
  }

  closeFab() {
    this.fabOpen = false;
  }

  ngOnInit() {
    setTimeout(() => { this.fabBtnPulse = false }, 5000);

    this.searchInput
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe(
      (query) => {
        this.query.query.title = query;
        this.query.pagination.page = 0;
        this.setState();
        this.getProjects();
      })
  }

  setState() {
    const modeTxt = this.query.query.active ? 'actual' : 'archive';
    this.location.replaceState(
      '/project/list/actual' + modeTxt + ';page=' + this.query.pagination.page + ';query=' + this.query.query.title);
  }

  changeProjectState(id: string) {
    this.projectService.update(id, { active: !this.query.query.active })
      .then((res) => {
        this.getProjects();
      }).catch(res => {
        this.projectService.handleError(res)
      })
  }

  removeItem(id: string) {
    this.projectService.delete(id)
      .then((res) => {
        this.getProjects();
      }).catch(res => {
        this.projectService.handleError(res)
      })
  }
}
