import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectComponent } from './project.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';

import { ProjectRoutingResolver } from './project-routing-resolver.service';

import { AuthGuard } from './../users/auth.guard';
import { CanDeactivateGuard } from './project-edit/can-deactivate-guard.service';

@NgModule({
  imports: [RouterModule.forChild(
    [
      {
        path: '',
        component: ProjectComponent,
        canActivateChild: [AuthGuard],
        children: [
          // { path: '', redirectTo: 'list' },
          { path: 'list/:mode', component: ProjectListComponent },
          { path: 'new', component: ProjectEditComponent },
          { path: 'edit/:id', component: ProjectEditComponent, canDeactivate: [CanDeactivateGuard] },
          { path: '**', redirectTo: 'list' }
        ]
      }
    ]
  )],
  providers: [
    ProjectRoutingResolver,
    CanDeactivateGuard
  ],
  exports: [RouterModule],

})
export class ProjectRoutingModule { }
