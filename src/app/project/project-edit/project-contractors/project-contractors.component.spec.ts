import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectContractorsComponent } from './project-contractors.component';

describe('ProjectEditComponent', () => {
  let component: ProjectContractorsComponent;
  let fixture: ComponentFixture<ProjectContractorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectContractorsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectContractorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
