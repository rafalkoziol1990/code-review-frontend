import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy, Input } from '@angular/core';


import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ExtendedFromControl, FormControlOptions } from './../../../shared/my-form/ExtendedFromControl';

import { ProjectService } from './../../project.service';
import { UsersService } from '../../../users/users.service';
import { Project } from './../../project';

@Component({
  selector: 'app-project-contractors',
  templateUrl: './project-contractors.component.html',
  styleUrls: ['./project-contractors.component.css']
})
export class ProjectContractorsComponent implements OnInit, OnDestroy {
  @Input('project') project: Project;
  private yourFriends: string[] = [];
  public contractors = [];
  public myForm: FormGroup;

  constructor(
    private usersService: UsersService,
    private projectService: ProjectService
  ) {
    usersService.user$
      .subscribe((user) => {
        console.log(user);
        if (user.friends) {
          this.yourFriends = user.friends;
        }
        if (!this.myForm) {
          this.myForm = this.newContractorForm();
        }
      })
  }

  ngOnInit() {
    this.prepContractorsList();
  }

  ngOnDestroy() {
    // this.usersService.user$.unsubscribe();
  }

  prepContractorsList() {
    const contractorsTempArr = [];
    if (this.project.contractors) {
      this.project.contractors.map((v, i) => {
        if (v.user !== '' && v.user.indexOf('@') !== -1) {
          const user = v.user.split('@');
          contractorsTempArr.push({
            sign: user[0].charAt(0).toUpperCase(),
            name: user[0],
            mail: v.user
          });
        }
        if (this.project.contractors.length === (i + 1)) {
          this.contractors = contractorsTempArr;
        }
      });
    }
  }

  newContractorForm(): FormGroup {
    return new FormGroup({
      contractor: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: `Contractor's mail`,
          placeholder: `Contractor's mail`,
          errorMsg: 'Field is required',
          type: 'autocomplete',
          tmp: this.yourFriends
        }),
        [Validators.required, Validators.email])
    })
  }

  updateInDB() {
    console.log({ contractors: this.project.contractors })
    this.projectService.update(this.project._id, { contractors: this.project.contractors })
      .then((res) => {
        this.prepContractorsList();
        this.myForm.reset();
      })
  }

  save() {
    const contractor = { user: this.myForm.controls.contractor.value };
    /* UPDATE friends list */
    this.usersService.addUserFriend(contractor.user).subscribe(
      (res) => {
        console.log(res)
      }
    );

    if (this.project.contractors) {
      this.project.contractors.push(contractor)
    } else {
      this.project.contractors = [contractor];
    }

    this.updateInDB();
  }

  remove(index) {
    this.project.contractors.splice(index, 1);
    this.updateInDB();
  }

  captureEvent(e) {
    console.log('e', e);
    if (e.key === 'Enter') {
      this.save();
    }
  }

}
