import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ExtendedFromControl, FormControlOptions } from './../../shared/my-form/ExtendedFromControl';

import { ProjectService } from './../project.service';
import { Project } from './../project';

import { exportedFxIteral } from './../../shared/fx.service';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit, OnDestroy {
  public appHeaderConfig = {
    displayHeader: false,
    style: {
      'background-color': 'rgba(255,255,255,1)',
      'color': 'rgba(0,0,0,0.87)'
    }
  };
  @Output() onAddNew = new EventEmitter<Project>();
  public project: Project;
  public myForm: FormGroup;
  private myFormValuesSubject = new Subject();

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService) {
    this.activatedRoute.params.subscribe((params: any) => {
      if (params.hasOwnProperty('id')) {
        // in edit mode
        this.projectService.fetchRow(params['id'])
          .subscribe((res) => {
            this.project = res;
            this.loadDataToForm();
            this.appHeaderConfig.displayHeader = true;
          })
      } else {
        // new mode
        this.projectService.add({}).subscribe(
          (res) => {
            this.project = res;
            this.loadDataToForm();
          });
      }
    })
  }

  ngOnInit() {
    this.myFormValuesSubject
      .debounceTime(300)
      .subscribe(
      (res) => {
        this.submit()
      })
  }

  ngOnDestroy() {
    this.onAddNew.emit(this.project);
    this.myFormValuesSubject.unsubscribe();
  }

  captureEvent($event) {
    this.myFormValuesSubject.next(this.myForm);
  }

  submit() {
    console.log('project', this.project)
    const postObj = {
      title: this.myForm.controls.title.value,
      description: this.myForm.controls.description.value,
      finish: this.myForm.controls.dates.value.finish
    }
    this.projectService.update(this.project._id, postObj).then(
      (res) => {
        console.log('update', res)
      });

    // console.log(this.myForm.controls.dates.controls)
    // this.projectService.add(postObj).subscribe(
    //   (res) => {

    //   });
  }


  loadDataToForm() {
    this.myForm = new FormGroup({
      title: new ExtendedFromControl(
        this.project.title,
        new FormControlOptions({
          label: 'Project title',
          placeholder: 'Enter project name',
          errorMsg: 'Field is required',
          type: 'text'
        }),
        [Validators.required, Validators.minLength(4), Validators.maxLength(128)]),
      description: new ExtendedFromControl(
        this.project.description,
        new FormControlOptions({
          label: 'Project description',
          placeholder: 'Enter project description',
          type: 'textarea'
        })),
      // Dates
      dates: new FormGroup({
        finish: new ExtendedFromControl(
          exportedFxIteral.dateToDateTimeLocal(this.project.finish),
          new FormControlOptions({
            label: 'Finish date',
            placeholder: 'Enter finish date',
            type: 'datetime-local'
          })),
      }),
    });
  }

}
