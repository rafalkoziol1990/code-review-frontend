import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ProjectEditComponent } from './project-edit.component';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {

  canDeactivate(
    component: CanComponentDeactivate,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> | boolean {
    // Get the ID
    console.log(route.paramMap.get('id'));

    // Get the current URL
    console.log(state.url);

    // Get the current component
    console.log(component);
    return new Promise((resolve) => {
      const notify = confirm('would you like to notify contributors?');
      if (notify) {
        alert('Notifications has not been implemented yet ;/ ')
        resolve(true);
      } else {
        resolve(true);
      }
    })

  }
}
