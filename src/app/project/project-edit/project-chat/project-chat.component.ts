import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy, Input, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExtendedFromControl, FormControlOptions } from './../../../shared/my-form/ExtendedFromControl';

import { ApiQueryModel } from './../../../shared/api-query-model';
import { ProjectService } from './../../project.service';
import { UsersService } from './../../../users/users.service';
import { Project } from './../../project';
import { User } from './../../../users/user';

@Component({
  selector: 'app-project-chat',
  templateUrl: './project-chat.component.html',
  styleUrls: ['./project-chat.component.css']
})
export class ProjectChatComponent implements OnInit, OnDestroy {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @Input('project') project: Project;
  public myForm: FormGroup = this.msgForm();
  private mySocket: WebSocket;
  private apiWsUrl: string;
  private jwt: string;
  private user: User;

  constructor(
    private projectService: ProjectService,
    private usersService: UsersService,
  ) {
    const headers = projectService.getHeaders();
    if (headers.has('access_token')) {
      this.jwt = headers.get('access_token');
    }
    this.apiWsUrl = projectService.getApiWsUrl();
    usersService.user$.subscribe(
      (res) => {
        this.user = res;
        console.log('user', this.user)
      });
    this.usersService.updateUserData();
  }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      }, 500)
    } catch (err) { }
  }

  ngOnInit() {
    this.mySocket = new WebSocket(this.apiWsUrl, [this.project._id, this.jwt]);

    this.mySocket.onopen = (event) => {
      console.log('websocket opened!', event);
    };

    this.mySocket.onmessage = (event) => {
      this.getMessages();
    }

    this.scrollToBottom();
  }

  ngOnDestroy() {
    this.mySocket.close();
  }

  getMessages() {
    this.projectService.fetchRow(this.project._id, new ApiQueryModel({ select: 'messages' }))
      .subscribe((res) => {
        this.project.messages = res.messages;
        this.scrollToBottom();
      })
  }

  msgForm(): FormGroup {
    return new FormGroup({
      msg: new ExtendedFromControl('', new FormControlOptions({
        extraClass: 'app-project-chat',
        label: '',
        placeholder: 'Message',
        type: 'text',
      }))
    })
  }

  sendMsg() {
    const msg = {
      data: this.myForm.controls.msg.value,
      jwt: this.jwt,
      project: this.project._id,
      user: {
        login: this.user.login,
        id: this.user._id
      }
    };
    this.mySocket.send(JSON.stringify(msg));
    this.myForm.reset();
  }

  captureEvent(e) {
    if (e.key === 'Enter') {
      this.sendMsg();
    }
  }
}
