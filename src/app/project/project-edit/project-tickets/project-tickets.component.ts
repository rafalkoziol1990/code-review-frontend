import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ExtendedFromControl, FormControlOptions } from './../../../shared/my-form/ExtendedFromControl';

import { ProjectService } from './../../project.service';
import { Project } from './../../project';

@Component({
  selector: 'app-project-tickets',
  templateUrl: './project-tickets.component.html',
  styleUrls: ['./project-tickets.component.css']
})
export class ProjectTicketsComponent implements OnInit, OnDestroy {
  @Input('project') project: Project;
  private featureSubject = new Subject<any>()
  public myForm: FormGroup;

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService) {

  }

  ngOnInit() {
    this.loadDataToForm();

    this.featureSubject
      .debounceTime(500)
      .subscribe((res) => {
        this.checkTickets();
      });
  }

  checkTickets() {
    const tickets = this.myForm.controls.tickets.value;
    const ticketsLength = tickets.length;
    if (tickets[ticketsLength - 1].content !== '' || ticketsLength === 0) {
      this.pushTicket(false, '');
    }
    this.saveTickets();
  }

  ngOnDestroy() {
    this.featureSubject.unsubscribe();
  }

  loadDataToForm() {
    this.myForm = new FormGroup({
      tickets: new FormArray([]),
    });
    this.project.tickets.map(
      res => {
        this.pushTicket(res.finished, res.content);
      })
    this.pushTicket(false, '');
  }

  setTicketItem(finished: boolean, content: string): FormGroup {
    return new FormGroup({
      finished: new ExtendedFromControl({ value: finished, disabled: false }, new FormControlOptions({
        disabled: false,
        extraClass: 'app-project-ticket app-project-ticket-finished',
        label: '',
        placeholder: '',
        type: 'checkbox',
      })),
      content: new ExtendedFromControl(content, new FormControlOptions({
        extraClass: 'app-project-ticket app-project-ticket-content',
        label: '',
        placeholder: 'Ticket content',
        type: 'text',
      }))
    })
  }

  pushTicket(finished: boolean, content: string) {
    const control = <FormArray>this.myForm.controls['tickets'];
    control.push(this.setTicketItem(finished, content));
  }

  saveTickets() {
    this.projectService.update(this.project._id, { tickets: this.myForm.controls.tickets.value.filter(v => v.content !== '') })
      .then((res) => {
        console.log(res);
      })
  }

  remove(index) {
    const control = <FormArray>this.myForm.controls['tickets'];
    control.removeAt(index);
    this.checkTickets();
  }

  captureEvent(e) {
    if (e.key === 'Backspace') {
      const tickets = this.myForm.controls.tickets.value;
      const ticketsLength = tickets.length;
      tickets.map((v, i) => {
        if (i !== ticketsLength - 1 && v.content === '') {
          this.remove(i);
        }
      })
    }
    this.featureSubject.next(this.myForm.controls.tickets);
  }

}
