import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  public data = [{
    title: 'Motivation',
    args: [
      'Learn Angular 4+',
      'Have some code to review on interview (can not show code from actual work)',
      'Create MEAN2 boilerplate']
  }, {
    title: 'Frontend',
    args: [
      'Extendable main api service based on generic types',
      'Reactive forms extended by extra features',
      'Lazy loading module',
      'Operations on backend are filtered by RxJs operators',
      'Guards in routes',
      'Atomic updates',
      'Lazy loading file',
      'UI by Material Design',
      'Animation based on Material movement guideline']
  }, {
    title: 'Backend',
    args: [
      'Server side based on ES6',
      'Routes in ExpressJs',
      'Authentication by JWT',
      'Secured by middlewares',
      'Upload with creating thumbs from media files (jpg, png, gif, mp4)',
      'Sending emails',
      'Buisnes logic separated from routers in controllers',
      'Database MongoDB and Mongoose',
      'Autogenerate sandbox for API',
      'JSDoc']
  }, {
    title: 'TO DO / TO IMPROVE',
    args: [
      'Write some unit tests',
      'Figure out way of editing one document by few users at once (block or stream updates)',
      'Work on detail of some animations',
      'To develop more complicated scenario there will be nice to use ngStore or implement own REDUX based on RxJs',
    ]
  }];

  constructor() { }

  ngOnInit() {
  }

  onSelect(event) {
    console.log(event);
  }

}
