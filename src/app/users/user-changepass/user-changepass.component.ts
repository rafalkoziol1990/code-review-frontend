import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import {
  FormArray,
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';

import { ExtendedFromControl, FormControlOptions } from './../../shared/my-form/ExtendedFromControl';

import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from './../users.service';
import { User } from './../user';

@Component({
  selector: 'app-user-changepass',
  templateUrl: './user-changepass.component.html',
  styleUrls: ['./user-changepass.component.css']
})
export class UserChangepassComponent implements OnInit {
  public myForm: FormGroup;
  public responseToTemplate: any = { inProgress: false, msg: '', status: undefined };

  constructor(public dialogRef: MdDialogRef<UserChangepassComponent>,
    @Inject(MD_DIALOG_DATA) public data: any,
    public sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private usersService: UsersService,
    private fb: FormBuilder
  ) {
    this.initForm();
  }

  ngOnInit() {

  }

  initForm(): void {
    this.myForm = new FormGroup({
      actualPassword: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: 'Actual password',
          placeholder: 'Actual password',
          errorMsg: 'Field must be specified!',
          type: 'password'
        }),
        [Validators.required, Validators.minLength(3)]),
      newPassword: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: 'New password',
          placeholder: 'New password',
          errorMsg: 'Field must be specified!',
          type: 'password'
        }),
        [Validators.required, Validators.minLength(3)]),
    });
    // this.myForm = this.fb.group({
    //   actualPassword: ['', [Validators.required, Validators.minLength(3)]],
    //   newPassword: ['', [Validators.required, Validators.minLength(3)]]
    // });
  }

  captureEvent(event) { }

  submit(controls): void {
    this.responseToTemplate.inProgress = true;
    this.usersService.changePassword({ password: controls.actualPassword.value, newPassword: controls.newPassword.value })
      .subscribe((res) => {
        console.log(res)
        if (res.status) {
          this.responseToTemplate.msg = 'Success!';
          this.responseToTemplate.status = true;
          setTimeout(() => {
            this.responseToTemplate.inProgress = false;
            this.dialogRef.close();
          }, 2500)
        } else {
          this.responseToTemplate.status = res.status;
          this.responseToTemplate.inProgress = false;
          this.responseToTemplate.msg = res.raport;
        }
      });
  }
}
