import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, Validators, FormBuilder } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from './../users.service';
import { User } from './../user';

import { ExtendedFromControl, FormControlOptions } from './../../shared/my-form/ExtendedFromControl';
@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
  public myForm: FormGroup;
  public responseToTemplate: any = { inProgress: false, msg: '', status: undefined };
  private user: User;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private usersService: UsersService,
    private fb: FormBuilder
  ) {
    this.initForm();
  }

  ngOnInit() {

  }

  initForm(): void {
    this.myForm = new FormGroup({
      email: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: 'Email',
          placeholder: 'Email',
          errorMsg: 'Field must be specified!',
          type: 'password'
        }),
        [Validators.email, Validators.required, Validators.minLength(3)]),
      password: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: 'Password',
          placeholder: 'Password',
          errorMsg: 'Field must be specified!',
          type: 'password'
        }),
        [Validators.required, Validators.minLength(3)]),
      passwordReplay: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: 'Replay password',
          placeholder: 'Replay password',
          errorMsg: 'Field must be specified!',
          type: 'password'
        }),
        [Validators.required, Validators.minLength(3)])
    })
  }

  captureEvent(event) { }

  submit(controls): void {
    this.responseToTemplate.inProgress = true;
    if (controls.password.value === controls.passwordReplay.value) {
      this.user = new User({ login: controls.email.value, password: controls.password.value });
      this.usersService.register(this.user)
        .subscribe((res) => {
          console.log(res)
          if (res.status) {
            this.responseToTemplate.msg = 'You created account! Sign in now!';
            this.responseToTemplate.status = true;
            this.responseToTemplate.inProgress = false;
            setTimeout(() => {
              this.router.navigate(['/user/login']);
            }, 1000)
          } else {
            this.responseToTemplate.status = res.status;
            this.responseToTemplate.inProgress = false;
            this.responseToTemplate.msg = res.raport;
          }
        });
    } else {
      this.responseToTemplate.inProgress = false;
      this.responseToTemplate.msg = 'Passwords do not match!';
      this.responseToTemplate.status = false;
    }
  }
}
