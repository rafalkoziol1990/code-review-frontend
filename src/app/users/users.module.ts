import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { MyDynamincFormModule } from './../shared/my-form/my-dynamic-form.module';

import { UsersRoutigModule } from './users-routig.module';

import { UsersComponent } from './users.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { UserRestoreComponent } from './user-restore/user-restore.component';
import { UserChangepassComponent } from './user-changepass/user-changepass.component';
import { UserSelectavatarComponent } from './user-selectavatar/user-selectavatar.component';
import { UserSetdataComponent } from './user-setdata/user-setdata.component';

import { FileModule } from './../shared/file/file.module';

import { UsersService } from './users.service';
import { MdlModule } from '@angular-mdl/core';
import { MaterialModule } from '@angular/material';
import { HeaderModule } from './../shared/header/header.module';

@NgModule({
  entryComponents: [UserSelectavatarComponent],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    UsersRoutigModule,
    MdlModule,
    FileModule,
    MaterialModule,
    HeaderModule,
    MyDynamincFormModule
  ],
  declarations: [
    UserChangepassComponent,
    UserLoginComponent,
    UserRegistrationComponent,
    UserRestoreComponent,
    UsersComponent,
    UserSelectavatarComponent,
    UserSetdataComponent
  ],
  providers: [UsersService],
  exports: []
})
export class UsersModule { }
