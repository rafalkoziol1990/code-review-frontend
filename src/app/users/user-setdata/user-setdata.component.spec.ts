import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSetdataComponent } from './user-setdata.component';

describe('UserSetdataComponent', () => {
  let component: UserSetdataComponent;
  let fixture: ComponentFixture<UserSetdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSetdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSetdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
