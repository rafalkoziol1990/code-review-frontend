import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from './../users.service';
import { User, Address } from './../user';

import { ExtendedFromControl, FormControlOptions } from './../../shared/my-form/ExtendedFromControl';


@Component({
  selector: 'app-user-setdata',
  templateUrl: './user-setdata.component.html',
  styleUrls: ['./user-setdata.component.css']
})
export class UserSetdataComponent implements OnInit, OnDestroy {
  public myForm: FormGroup;
  public responseToTemplate: any = { inProgress: false, msg: '', status: undefined };
  public appHeaderConfig = {
    displayHeader: true,
    style: {
      'background-color': 'rgba(255,255,255,1)',
      'color': 'rgba(0,0,0,0.87)'
    }
  };
  public formTxt = {
    header: 'Set property value',
    description: 'Customize Your account with your personal data!'
  };
  private user: User = new User({});
  private mode: string;
  private fieldsToUpdate = new Subject<any>()

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private usersService: UsersService,
    private fb: FormBuilder
  ) {
    this.activatedRoute.params.subscribe((params) => {
      if (params.hasOwnProperty('mode')) {
        this.mode = params['mode'];
        this.usersService.getUserData().subscribe((res) => {
          this.user = res;
          this.initForm();
        });
      }
    });
  }

  ngOnInit() {
    this.fieldsToUpdate
      .debounceTime(900)
      .distinctUntilChanged()
      .subscribe((res) => {
        this.submit(res);
      });
  }

  ngOnDestroy() {
    this.fieldsToUpdate.unsubscribe();
  }

  initForm(): void {
    if (this.mode === 'names') {
      this.myForm = new FormGroup({
        firstname: new ExtendedFromControl(
          this.user.firstname,
          new FormControlOptions({ label: 'Firstname', placeholder: 'Enter firstname', errorMsg: 'Field is required' }),
          [Validators.required]),
        lastname: new ExtendedFromControl(
          this.user.lastname,
          new FormControlOptions({ label: 'Lastname', placeholder: 'Enter lastname', errorMsg: 'Field is required' }),
          [Validators.required]),
      });
    } else if (this.mode === 'phone') {
      this.myForm = new FormGroup({
        phone: new ExtendedFromControl(
          this.user.phone,
          new FormControlOptions({ label: 'Phone', placeholder: 'Enter phone number', errorMsg: 'Field is required' }),
          [Validators.required]),
      });
    } else if (this.mode === 'address') {
      const address = this.user.address !== undefined ? this.user.address : new Address({});
      this.myForm = new FormGroup({
        post: new ExtendedFromControl(
          address.post,
          new FormControlOptions({ label: 'Post', placeholder: 'Enter Your post', errorMsg: 'Field is required' }),
          [Validators.required]),
        city: new ExtendedFromControl(
          address.city,
          new FormControlOptions({ label: 'City', placeholder: 'Enter Your city', errorMsg: 'Field is required' }),
          [Validators.required]),
        street: new ExtendedFromControl(
          address.street,
          new FormControlOptions({ label: 'Street', placeholder: 'Enter Your street', errorMsg: 'Field is required' }),
          [Validators.required]),
      });
    }
  };

  captureEvent(data) {
    let fieldsToUpdate = {};
    if (this.mode === 'names') {
      fieldsToUpdate = { 'firstname': this.myForm.controls.firstname.value, 'lastname': this.myForm.controls.lastname.value }
    } else if (this.mode === 'phone') {
      fieldsToUpdate = { 'phone': this.myForm.controls.phone.value };
    } else if (this.mode === 'address') {
      fieldsToUpdate = {
        'address': {
          'post': this.myForm.controls.post.value,
          'city': this.myForm.controls.city.value,
          'street': this.myForm.controls.street.value
        }
      };
    };
    this.fieldsToUpdate.next(fieldsToUpdate);
  };

  submit(values): void {
    // console.log(this.myForm.controls)
    this.responseToTemplate.inProgress = true;
    this.responseToTemplate.msg = 'Saveing ...';

    this.usersService.changeUserData(values)
      .subscribe((res) => {
        this.responseToTemplate.msg = 'Saved !';
        this.responseToTemplate.status = true;
        setTimeout(() => {
          this.responseToTemplate.msg = '';
          this.responseToTemplate.inProgress = false;
        }, 2500);
      });
  }
}
