import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { UserRestoreComponent } from './user-restore/user-restore.component';
import { UserChangepassComponent } from './user-changepass/user-changepass.component';
import { UserSetdataComponent } from './user-setdata/user-setdata.component';
import { AuthGuard } from './auth.guard';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: 'user', children: [
        { path: '', component: UsersComponent, canActivate: [AuthGuard] },
        { path: 'account-settings', component: UsersComponent, canActivate: [AuthGuard] },
        { path: 'login', component: UserLoginComponent },
        { path: 'registry', component: UserRegistrationComponent },
        { path: 'restore-password', component: UserRestoreComponent },
        { path: 'change-password', component: UserChangepassComponent, canActivate: [AuthGuard] },
        { path: 'set-data/:mode', component: UserSetdataComponent, canActivate: [AuthGuard] },
        { path: '**', redirectTo: '' }
      ]
    }
  ]),
  ],
  exports: [RouterModule]
})
export class UsersRoutigModule { }
