import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, CanLoad } from '@angular/router';

import { UsersService } from './users.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

    constructor(private router: Router, private usersService: UsersService) { }

    canLoad() {
        return this.canActivatePromise();
    }

    canActivate() {
        return this.canActivatePromise();
    }

    canActivateChild() {
        return this.canActivatePromise();
    }

    canActivatePromise() {
        return new Promise((resolve) => {
            this.usersService.isAuthenticate().subscribe(
                (res) => {
                    if (res.status) {
                        this.usersService.setAuthStatus(true);
                        resolve(true);
                    } else {
                        this.usersService.setAuthStatus(false);
                        this.router.navigate(['/user/login']);
                        resolve(false);
                    }
                });
        });
    }
}
