import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from './../users.service';
import { User } from './../user';

import { ExtendedFromControl, FormControlOptions } from './../../shared/my-form/ExtendedFromControl';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  public myForm: FormGroup;
  public responseToTemplate: any = { inProgress: false, msg: '', status: undefined };
  private user: User;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private usersService: UsersService,
    private fb: FormBuilder
  ) {
    console.log('user-login component!');
    this.initForm();
  }

  ngOnInit() {

  }

  initForm(): void {
    this.myForm = new FormGroup({
      email: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: 'Email',
          placeholder: 'Email',
          errorMsg: 'Field must be specified!',
          type: 'email'
        }),
        [Validators.email, Validators.required, Validators.minLength(3)]),
      password: new ExtendedFromControl(
        '',
        new FormControlOptions({
          label: 'Password',
          placeholder: 'Password',
          errorMsg: 'Field must be specified!',
          type: 'password'
        }),
        [Validators.required, Validators.minLength(3)]),
    })
    // this.myForm = this.fb.group({
    //   email: ['', [Validators.required, Validators.minLength(5), Validators.email]],
    //   password: ['', [Validators.required, Validators.minLength(3)]]
    // });
  }

  captureEvent(event) { }

  submit(controls): void {
    this.responseToTemplate.inProgress = true;
    this.user = new User({ login: controls.email.value, password: controls.password.value });
    this.usersService.login(this.user)
      .subscribe((res) => {
        console.log(res)
        if (res.status) {
          this.responseToTemplate.msg = 'Success!';
          this.responseToTemplate.status = true;
          this.responseToTemplate.inProgress = false;
          setTimeout(() => {
            this.router.navigate(['/']);
          }, 1000)
        } else {
          this.responseToTemplate.status = res.status;
          this.responseToTemplate.inProgress = false;
          this.responseToTemplate.msg = res.raport ? res.raport : 'Login failed ...';
        }
      });
  }

}
