export class Address {
    public post: string;
    public code: string;
    public street: string;

    constructor(params: { post?: string, code?: string, street?: string }) {
        this.post = params.post || '';
        this.code = params.code || '';
        this.street = params.street || '';
    }
}

export class User {
    public _id: string;
    public avatar: string;
    public login: string;
    public firstname: string;
    public lastname: string;
    public phone: string;
    public password: string;
    public address: any;
    public access_token: string;
    public data: any;
    public friends: string[];

    constructor(params: {
        _id?: string,
        avatar?: string,
        login?: string,
        firstname?: string,
        lastname?: string,
        phone?: string,
        password?: string,
        address?: Address,
        access_token?: string,
        data?: any,
        friends?: string[]
    }) {
        this._id = params._id || '';
        this.avatar = params.avatar || '';
        this.login = params.login || '';
        this.firstname = params.firstname || '';
        this.lastname = params.lastname || '';
        this.phone = params.phone || '';
        this.password = params.password || '';
        this.address = params.address || new Address({});
        this.access_token = params.access_token || '';
        this.data = params.data || {};
    };
}

