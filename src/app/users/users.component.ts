import { Component } from '@angular/core';
import { MdDialogModule, MdDialog, MdDialogRef } from '@angular/material';

import { UserChangepassComponent } from './user-changepass/user-changepass.component';
import { UserSelectavatarComponent } from './user-selectavatar/user-selectavatar.component';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { UsersService } from './users.service';
import { User } from './user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  public user: User = new User({});
  public avatar: any = { url: '', sign: '' };

  constructor(public avatarDialog: MdDialog,
    public changePassDialog: MdDialog,
    private usersService: UsersService,
    private router: Router) {
    this.usersService.updateUserData();
    this.usersService.user$
      .subscribe((res) => {
        this.user = res;
        this.avatar = {
          url: this.usersService.getApiUrl() + 'public/uploads/' + this.user._id + '/userAvatar/' + this.user._id + '/s/' + this.user.avatar,
          sign: this.user.login.charAt(0).toUpperCase()
        };
      });
  }

  setProperty(property) {
    this.router.navigate(['/user/set-data', property]);
  }

  selectAvatar() {
    let avatarDialogRef = this.avatarDialog.open(UserSelectavatarComponent, {
      data: { user: this.user },
      width: '90vw',
      height: '90vh',
    });
  }

  changePassword() {
    let changePassDialogRef = this.changePassDialog.open(UserChangepassComponent, {
      data: { user: this.user }
    });
  }

}
