import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { User } from './user';

import { ApiService } from './../shared/api.service';

@Injectable()
export class UsersService extends ApiService<User> {
  private servicePostfix = 'user';
  private userUrl = '';
  // Observable User sources
  private isAuth = new Subject<boolean>();
  private user = new Subject<User>();
  // Observable user streams
  isAuth$ = this.isAuth.asObservable();
  user$ = this.user.asObservable();

  constructor(http: Http, private router: Router) {
    super(http);
    this.setModuleName(this.servicePostfix);
    this.userUrl = this.getApiUrl() + this.getModuleName();
  }

  // Service message commands
  setAuthStatus(isAuth: boolean) {
    this.isAuth.next(isAuth);
    this.updateUserData();
  }

  login(user: User): Observable<any> {
    return this.getHttp().post(this.userUrl + '/login',
      { login: user.login, password: user.password },
      new RequestOptions({ headers: this.getHeaders() }))
      .map((res: any) => {
        if (res.json().status) {
          localStorage.setItem('access_token', res.json().access_token);
          this.setAuthStatus(true);
        }
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  register(user: User): Observable<any> {
    return this.getHttp().post(this.userUrl + '/registry',
      { login: user.login, password: user.password },
      new RequestOptions({ headers: this.getHeaders() }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  restorePassword(user: User): Observable<any> {
    return this.getHttp().post(this.userUrl + '/restorePassword', { login: user.login }, new RequestOptions({ headers: this.getHeaders() }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  changePassword(changePassObj: any): Observable<any> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.getHttp().post(this.userUrl + '/changePassword',
      { password: changePassObj.password, newPassword: changePassObj.newPassword },
      new RequestOptions({ headers: this.getHeaders() }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  changeUserData(changeUserData: any): Observable<any> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.getHttp().post(this.userUrl + '/changeUserData', { userData: JSON.stringify(changeUserData) },
      new RequestOptions({ headers: this.getHeaders() }))
      .map((res) => {
        this.updateUserData();
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  addUserFriend(friendLogin: String): Observable<any> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.getHttp().post(this.userUrl + '/addNewFriend', { friend: friendLogin },
      new RequestOptions({ headers: this.getHeaders() }))
      .map((res) => {
        this.updateUserData();
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  logout() {
    localStorage.setItem('access_token', '');
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    this.router.navigate(['/user/login']);
    this.setAuthStatus(false);
  }

  isAuthenticate(): Observable<any> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.getHttp().get(this.userUrl + '/isAuthenticate', new RequestOptions({ headers: this.getHeaders() }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  getUserData(): Observable<any> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.getHttp().get(this.userUrl + '/getUserData', new RequestOptions({ headers: this.getHeaders() }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  updateUserData() {
    this.getUserData()
      .subscribe((res) => {
        this.user.next(res);
      });
  }

}
