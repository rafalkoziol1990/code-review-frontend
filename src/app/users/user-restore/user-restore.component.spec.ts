import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRestoreComponent } from './user-restore.component';

describe('UserRestoreComponent', () => {
  let component: UserRestoreComponent;
  let fixture: ComponentFixture<UserRestoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRestoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRestoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
