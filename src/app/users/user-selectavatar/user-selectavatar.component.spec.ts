import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSelectavatarComponent } from './user-selectavatar.component';

describe('UserChangepassComponent', () => {
  let component: UserSelectavatarComponent;
  let fixture: ComponentFixture<UserSelectavatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserSelectavatarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSelectavatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
