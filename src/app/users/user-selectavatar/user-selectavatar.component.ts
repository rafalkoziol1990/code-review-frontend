import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

import { UsersService } from './../users.service';
import { User } from './../user';

@Component({
  selector: 'app-user-selectavatar',
  templateUrl: './user-selectavatar.component.html',
  styleUrls: ['./user-selectavatar.component.css']
})
export class UserSelectavatarComponent implements OnInit {
  public selectedFile = null;
  public userId;

  constructor(public dialogRef: MdDialogRef<UserSelectavatarComponent>,
    @Inject(MD_DIALOG_DATA) public data: any,
    public sanitizer: DomSanitizer,
    private usersService: UsersService,
  ) {
    this.usersService.getUserData().subscribe((user) => {
      this.userId = user._id;
    })
  }

  ngOnInit() { }

  onSelect(file) {
    this.selectedFile = file;
  }

  confirmSelect() {
    this.updateUserAvatar();
  }

  cancelSelect() {
    this.selectedFile = null;
    this.dialogRef.close();
  }

  updateUserAvatar() {
    this.usersService.changeUserData({ avatar: this.selectedFile.filename })
      .subscribe((res) => {
        this.usersService.updateUserData();
        this.dialogRef.close();
      });
  }



}
