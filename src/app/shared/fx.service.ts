const fx = {
  dateToDateTimeLocal: (dv: any): any => {
    const date = new Date(dv);
    const prepareDataValue = (val: any): any => {
      return val < 10 ? '0' + val : val;
    }
    return date.getFullYear() + '-' + prepareDataValue(date.getMonth() + 1) + '-' + prepareDataValue(date.getDate())
      + 'T' + prepareDataValue((date.getHours() - 1)) + ':' + prepareDataValue(date.getMinutes());
  }
}

export const exportedFxIteral = fx;


