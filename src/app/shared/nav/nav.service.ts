import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { UsersService } from './../../users/users.service';
import { User } from './../../users/user';
@Injectable()
export class NavService {
  public user: User = new User({});
  public avatar: any = { url: '', sign: '' };
  // Observable Nav sources
  private nav = new Subject<any>();
  // Observable Nav streams
  nav$ = this.nav.asObservable();

  constructor() {
  }

  getNav(): any {
    return this.nav;
  }

  setNav(nav: any): void {
    this.nav.next(nav);
  }
}
