import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule, PreloadAllModules } from '@angular/router';

import { MdlModule } from '@angular-mdl/core';
import { MaterialModule } from '@angular/material';

import { NavComponent } from './nav.component';
import { NavService } from './nav.service';

@NgModule({
  entryComponents: [],
  imports: [
    CommonModule,
    MdlModule,
    MaterialModule,
    RouterModule
  ],
  declarations: [
    NavComponent, 
  ],
  providers: [NavService],
  exports: [NavComponent]
})
export class NavModule { }
