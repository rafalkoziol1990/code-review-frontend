import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavService } from './nav.service';
import { UsersService } from './../../users/users.service';
import { User } from './../../users/user';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {
  public nav: any;
  public isAuth: boolean;
  public user: User = new User({});
  public avatar: any = { url: '', sign: '' };

  constructor(private usersService: UsersService, private navService: NavService) {
    // this.usersService.updateUserData();
    this.usersService.user$
      .subscribe((res) => {
        if (res.hasOwnProperty('login')) {
          this.user = res;
          this.avatar = {
            url: this.usersService.getApiUrl() + 'public/uploads/' + this.user._id + '/userAvatar/' + this.user._id + '/s/' + this.user.avatar,
            sign: this.user.login.charAt(0).toUpperCase()
          };
        } else {
          this.avatar = { url: undefined, sign: undefined };
        }
        // this.setNav(undefined, undefined);
      });
    usersService.isAuth$.subscribe((res) => {
      this.isAuth = res;
      // this.setNav(undefined, undefined);
    });
    navService.nav$.subscribe((res) => {
      this.nav = res;
    });
  }

  closeNav() {
    // alert('close')
    this.setNav(true, true);
  }

  setNav(open, needToClose) {
    this.nav = {
      isAuth: this.isAuth,
      user: this.user,
      avatar: this.avatar,
      sideNav: {
        open: open,
        needToClose: needToClose
      },
      mainHeader: {
        visible: true
      }
    };
    this.navService.setNav(this.nav);
  }

  SignOut() {
    this.closeNav();
    this.usersService.logout();
  }

  ngOnInit() {
  }
}
