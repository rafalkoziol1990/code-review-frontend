import { FormControl, Validators, ValidatorFn, AsyncValidatorFn } from '@angular/forms';

export class FormControlOptions {
  public label: string;
  public type: string;
  public placeholder: string;
  public errorMsg: string;
  public extraClass: string;
  public disabled: boolean;
  public tmp: any;
  constructor(params: {
    label?: string,
    type?: string,
    disabled?: boolean,
    placeholder?: string,
    errorMsg?: string,
    extraClass?: string,
    tmp?: any,
  }) {
    this.label = params.label || '';
    this.type = params.type || 'text';
    this.disabled = params.disabled || false;
    this.placeholder = params.placeholder || '';
    this.errorMsg = params.errorMsg || '';
    this.extraClass = params.extraClass || '';
    this.tmp = params.tmp || {};
  };
}

export class ExtendedFromControl extends FormControl {
  public controlOptions: FormControlOptions = null;

  constructor(formState?: any,
    options?: FormControlOptions,
    validator?: ValidatorFn | ValidatorFn[] | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null) {
    super(formState, validator, asyncValidator)
    this.controlOptions = options;
  }
}
