import { Component, Inject, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { ExtendedFromControl, FormControlOptions } from './../ExtendedFromControl';

import {
  FormArray,
  FormGroup,
  Validators,
  FormControl,
  FormBuilder
} from '@angular/forms';

@Component({
  selector: 'my-dynamic-form-group',
  templateUrl: './my-dynamic-form-group.component.html',
  styleUrls: ['./my-dynamic-form-group.component.css']
})
export class MyDynamicFormGroupComponent implements OnInit, OnChanges {
  @Input('extraClass') extraClass: string;
  private form: FormGroup = null;
  @Input('dynamicFormGroup') set myForm(newForm: FormGroup) {
    this.form = newForm;
  }
  get myForm(){
    return this.form;
  }
  @Output() onInputChange = new EventEmitter<any>();
  public formControls: any[] = [];

  ngOnInit() {
    // console.log(this.myForm);
    // this.setForm();
  }

  ngOnChanges() {
    this.setForm();
  }

  setForm() {
    this.formControls = [];
    Object.keys(this.myForm.controls).forEach((v, i) => {
      const control = this.myForm.controls[v];
      if (control instanceof ExtendedFromControl) {
        this.formControls.push({
          key: v,
          value: control
        });
      }
    });
  }

  onChange($event) {
    this.onInputChange.emit($event);
  }

  onKeyUp($event) {
    this.onInputChange.emit($event);
  }

}
