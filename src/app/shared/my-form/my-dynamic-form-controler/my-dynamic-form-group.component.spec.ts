import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDynamicFormGroupComponent } from './my-dynamic-form-group.component';

describe('MyDynamicFormGroupComponent', () => {
  let component: MyDynamicFormGroupComponent;
  let fixture: ComponentFixture<MyDynamicFormGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDynamicFormGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDynamicFormGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
