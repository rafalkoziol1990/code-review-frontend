import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdlModule } from '@angular-mdl/core';
import { MaterialModule, OverlayModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDynamicFormGroupComponent } from './my-dynamic-form-controler/my-dynamic-form-group.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MaterialModule,
    MdlModule,
  ],
  declarations: [
    MyDynamicFormGroupComponent,
  ],
  exports: [
    MyDynamicFormGroupComponent
  ]
})
export class MyDynamincFormModule { }
