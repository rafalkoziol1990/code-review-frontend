import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
// RxJs
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import { Observable, Subject } from 'rxjs/Rx';

import { ApiQueryModel } from './api-query-model';

export interface IFetchAllReturnObj<T> {
  data: T[],
  query: any
}

@Injectable()
export class ApiService<T> {
  private moduleName = ''; // example 'task'
  private apiUrl = 'http://localhost:8080/';
  private apiWsUrl = 'ws://localhost:5000';
  // private apiUrl = 'http://137.74.82.193:8080/';
  // private apiWsUrl = 'ws://137.74.82.193:5000';
  private headers: Headers = new Headers({ 'Content-Type': 'application/json' });

  getApiUrl(): string {
    return this.apiUrl;
  }

  getApiWsUrl(): string {
    return this.apiWsUrl;
  }

  getModuleName(): string {
    return this.moduleName;
  }

  setModuleName(moduleName: string): void {
    this.moduleName = moduleName;
  }

  getHeaders(): Headers {
    return this.headers;
  }

  setHeaders(name: any, value: any) {
    this.headers.set(name, value);
    // this.headers.append(name, value);
  }

  getHttp() {
    return this.http;
  }

  constructor(private http: Http) {
    console.log('API RISE')
    this.setHeaders('access_token', localStorage.getItem('access_token'));
  }

  add(data: any): Observable<T> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.http.post(this.apiUrl + this.moduleName + '/add', { data }, new RequestOptions({ headers: this.headers }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  fetchRow(id: string, query?: ApiQueryModel): Observable<T> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.http.post(this.apiUrl + this.moduleName + '/fetchRow/' + id, { query }, new RequestOptions({ headers: this.headers }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  // fetchAll(query?: ApiQueryModel): Observable<T[]> {
  //   return this.http.post(this.apiUrl + this.moduleName + '/fetchAll', query, this.headers)
  //     .map((res) => {
  //       return this.handleRes(res);
  //     })
  //     .catch(this.handleError);
  // }

  fetchAll(query?: ApiQueryModel): Observable<IFetchAllReturnObj<T>> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.http.post(this.apiUrl + this.moduleName + '/fetchAll', query, new RequestOptions({ headers: this.headers }))
      .map((res) => {
        return this.handleRes(res);
      })
      .catch(this.handleError);
  }

  update(id: string, data: any): Promise<T> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.http.post(this.apiUrl + this.moduleName + '/update/' + id, data, new RequestOptions({ headers: this.headers }))
      .toPromise()
      .then((res) => {
        return this.handleRes(res)
      })
      .catch(this.handleError);
  }

  delete(id: string): Promise<{ status: boolean, raport: string }> {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    return this.http.delete(this.apiUrl + this.moduleName + '/delete/' + id, new RequestOptions({ headers: this.headers }))
      .toPromise()
      .then((res) => {
        return this.handleRes(res)
      })
      .catch(this.handleError);
  }

  handleRes(res: Response) {
    return res.json();
  }

  handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
