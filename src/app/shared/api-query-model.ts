interface Pagination {
  page: number;
  limit: number;
  total: number;
}

export class ApiQueryModel {
  query: any;
  pagination: Pagination;
  sort: Object | string;
  select?: Object | number;
  populate?: string;

  constructor(options: {
    query?: any,
    pagination?: Pagination,
    sort?: Object | string,
    select?: Object | number,
    populate?: string,
  }) {
    this.query = options.query || {};
    this.pagination = options.pagination || {
      page: 0,
      limit: 10,
      total: undefined,

    };
    this.sort = options.sort || '-createdAt';
    this.select = options.select || '';
    this.populate = options.populate || '';
  }
}
/*
var a: any;
var b: Object;
var c: {};

a has no interface, it can be anything, the compiler knows nothing about its members so minimal type 
checking is done when accessing/assigning both to itself and its members. Basically, you're telling the compiler 
to "back off, I know what I'm doing, so just trust me";

b has the Object interface, so ONLY the members defined in that interface are available for b. It's still JavaScript, 
so everything extends Object;

c extends Object, like anything else in TypeScript, but add no members. Since type compatibility in 
TypeScript is based on structural subtyping, not nominal subtyping, c ends up being the same as b 
because they have the same interface: the Object interface.
 */