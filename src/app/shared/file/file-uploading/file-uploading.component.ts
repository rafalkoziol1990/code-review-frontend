import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/core';
import { FileService, UploadingFileModel } from './../file.service';

@Component({
  selector: 'app-file-uploading',
  templateUrl: './file-uploading.component.html',
  styleUrls: ['./file-uploading.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('uloadingFileAnimation', [
      state('*', style({
        opacity: '1',
        transform: 'translate3d(0, 0, 0) scale3d(1, 1, 1)',
      })),
      state('void', style({
        opacity: '0.1',
        transform: 'translate3d(0, 100vh, 0) scale3d(0.1, 0.1, 0.1)',
      })),
      transition('void => *', animate('375ms 10ms cubic-bezier(0.0, 0.0, 0.2, 1)')),
      transition('* => void', animate('375ms 10ms cubic-bezier(0.0, 0.0, 0.2, 1)'))
    ])
  ]
})
export class FileUploadingComponent implements OnInit {
  public uploadingFiles: UploadingFileModel[] = [];

  constructor(
    private fileService: FileService,
    private cdr: ChangeDetectorRef
  ) {
    fileService.uploadingFiles$.subscribe(
      (res) => {
        this.uploadingFiles = res;
        this.cdr.detectChanges();
        this.cdr.markForCheck();
      }
    );
  }

  ngOnInit() {

  }

  clearUloadingList(): void {
    this.uploadingFiles = [];
  }
}

