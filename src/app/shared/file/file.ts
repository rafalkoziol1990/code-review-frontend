export class File {
    public _id: string;
    public filename: string;
    public destination: string;
    public originalname: string;
    public mimetype: string;
    public size: number;
    public keyword: string;
    public parentid: string;
    public info: any;
    public state: string;
    public userid: string;
    public tmp: any;

    constructor(option: {
        _id?: string,
        filename?: string,
        destination?: string,
        originalname?: string,
        mimetype?: string,
        size?: number,
        keyword?: string,
        parentid?: string,
        info?: any,
        state?: string,
        userid?: string
        tmp?: any
    }) {
        this._id = option._id || '';
        this.filename = option.filename || '';
        this.destination = option.destination || '';
        this.originalname = option.originalname || '';
        this.mimetype = option.mimetype || '';
        this.size = option.size || 0;
        this.keyword = option.keyword || '';
        this.parentid = option.parentid || '';
        this.info = option.info || {};
        this.state = option.state || '';
        this.userid = option.userid || '';
        this.tmp = option.tmp || '';
    };
}
