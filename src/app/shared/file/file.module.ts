import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { FileComponent } from './file.component';

import { FileSizePipe } from './file-size.pipe';

import { FileService } from './file.service';
import { FileUploadingComponent } from './file-uploading/file-uploading.component';
import { FilePreviewComponent } from './file-preview/file-preview.component';

import { MdlModule } from '@angular-mdl/core';

@NgModule({
  entryComponents: [FilePreviewComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MdlModule,
  ],
  declarations: [
    FileComponent,
    FileUploadingComponent,
    FilePreviewComponent,
    FileSizePipe,
  ],
  providers: [FileService],
  exports: [
    FileComponent,
    FileUploadingComponent
  ]
})
export class FileModule { }
