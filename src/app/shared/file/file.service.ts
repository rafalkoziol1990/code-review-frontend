import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import { Observable, Subject } from 'rxjs/Rx';

import { File } from './file';
import { ApiQueryModel } from './../api-query-model';
import { ApiService } from './../api.service';

export class UploadingFileModel {
  public file: any;
  public progress: number;
  public error: boolean;
  public finished: boolean;
  public status: string;
  public readyToDisplay: boolean;

  constructor(options: {
    file?: any,
    progress?: number,
    error?: boolean,
    finished?: boolean,
    status?: string,
    readyToDisplay?: boolean
  }) {
    this.file = options.file || '';
    this.progress = options.progress || 0;
    this.error = options.error || false;
    this.finished = options.finished || false;
    this.status = options.status || 'uploading';
    this.readyToDisplay = options.readyToDisplay || false;
  }
}

@Injectable()
export class FileService extends ApiService<File> {
  private servicePostfix: string = 'file';
  private query = new ApiQueryModel({});
  private debouncedQuery = new Subject<any>();
  // variables
  private uploadingFiles: UploadingFileModel[] = [];
  private files: File[] = [];
  // Observable string sources
  private uploadingFilesSubject = new Subject<UploadingFileModel[]>();
  private filesSubject = new Subject<File[]>();
  // Observable string streams
  uploadingFiles$ = this.uploadingFilesSubject.asObservable();
  files$ = this.filesSubject.asObservable();

  constructor(http: Http) {
    super(http);
    this.setModuleName(this.servicePostfix);
    this.initDebounceQuery();
  }

  getQuery() {
    return this.query;
  }

  clearUploadData(): void {
    this.uploadingFiles = [];
  }

  uploadData(file: any, index: number, parentid: string, keyword: string): void {
    this.uploadingFiles[index] = new UploadingFileModel({ file: file, finished: false, status: 'uploading' });
    /* Delay 4 animation */
    const formData = new FormData();
    formData.append('file', file);

    const xhr = new XMLHttpRequest();
    xhr.upload.onprogress = (e: ProgressEvent) => {
      if (e.lengthComputable) {
        this.uploadingFiles[index].progress = e.loaded / e.total;
        this.uploadingFilesSubject.next(this.uploadingFiles);
      }
    };
    xhr.upload.onabort = (e: Event) => {
      this.uploadingFiles[index].error = true;
      this.uploadingFilesSubject.next(this.uploadingFiles);
    };
    xhr.upload.onerror = (e: Event) => {
      this.uploadingFiles[index].error = true;
      this.uploadingFilesSubject.next(this.uploadingFiles);
    };
    xhr.upload.onloadend = (e: Event) => {
      this.uploadingFiles[index].finished = true;
      this.uploadingFiles[index].status = 'uploaded';
      this.uploadingFilesSubject.next(this.uploadingFiles);
      this.setDebouncedQuery(parentid, keyword, 0);
    };
    xhr.onreadystatechange = () => {
      this.uploadingFilesSubject.next(this.uploadingFiles);
      console.log(this.uploadingFiles)
      // this.getFiles(parentid, keyword, 0);
    };

    const xhrUrl = this.getApiUrl() + this.servicePostfix + '/upload?parentid=' + parentid + '&keyword=' + keyword;
    xhr.open('POST', xhrUrl);
    xhr.setRequestHeader('access_token', localStorage.getItem('access_token'));
    xhr.send(formData);
  }

  initDebounceQuery(): any {
    this.debouncedQuery
      // .throttleTime(1500)
      // .distinctUntilChanged()
      .debounceTime(900)
      .subscribe((res) => {
        this.getFiles(res.parentid, res.keyword, res.page)
      })
  }

  setDebouncedQuery(parentid, keyword, page): void {
    this.debouncedQuery.next({ parentid: parentid, keyword: keyword, page: page })
  }

  getFiles(parentid, keyword, page): any {
    this.setHeaders('access_token', localStorage.getItem('access_token'));
    this.query.pagination = { limit: 24, page: page, total: null };
    this.query.query = { parentid: parentid, keyword: keyword };
    this.fetchAll(this.query)
      .map((res) => {
        this.files = res.data;
        this.query = res.query;
        this.filesSubject.next(this.files);
      })
      .subscribe();
  }
}
