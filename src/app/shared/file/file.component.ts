import {
  Component,
  OnInit, OnDestroy,
  Input, Output, EventEmitter,
  trigger, animate, transition, style, state,
  ElementRef, ViewChild, HostListener
} from '@angular/core';
import { FileService, UploadingFileModel } from './file.service';
import { ApiQueryModel } from './../api-query-model';
import { File } from './file';
import { MdDialogModule, MdDialog, MdDialogRef, MdDialogConfig, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { FilePreviewComponent } from './file-preview/file-preview.component';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css'],
  animations: [
    trigger('previewAnimation', [

    ]),
  ]
})
export class FileComponent implements OnInit, OnDestroy {
  @Input('keyword') keyword: string;
  @Input('parentid') parentid: string;
  @Input('mode') mode: string;
  @Output() onSelect = new EventEmitter<any>();

  public selectedFile: File = null;
  public files: File[];
  public fileUrl = '';
  public iconUrl = 'http://localhost:8080/public/icons/';
  public query: ApiQueryModel;
  public elementProp: any = {};
  @ViewChild('uploadedfiles') uploadedFilesParentDom: ElementRef;

  constructor(private fileService: FileService,
    private el: ElementRef,
    public dialog: MdDialog,
    public snackBar: MdSnackBar) {
    this.fileUrl = this.fileService.getApiUrl();
    fileService.files$
      .subscribe(
      (res) => {
        this.elementProp.showLoader = false;
        this.query = this.fileService.getQuery();
        if (this.query.pagination.page > 0) {
          this.files.push(...res);
        } else {
          this.files = res;
        }
        this.files.forEach((v, i) => {
          if (v.mimetype === 'image/jpeg' || v.mimetype === 'image/png' || v.mimetype === 'image/gif' || v.mimetype === 'video/mp4' || v.mimetype === 'video/avi') {
            this.files[i].tmp = { thumb: this.fileUrl + v.destination + '/s/' + v.filename };
          } else if (v.mimetype === 'application/pdf') {
            this.files[i].tmp = { thumb: this.iconUrl + 'picture_as_pdf.png' };
          } else if (v.mimetype === 'audio/mp3') {
            this.files[i].tmp = { thumb: this.iconUrl + 'music_note.png' };
          } else {
            this.files[i].tmp = { thumb: this.iconUrl + 'insert_drive_file.png' };
          }
          this.files[i].tmp.delay = (i % this.query.pagination.limit) + 1;
        });
      });
  }

  ngOnInit() {
    this.getAll(0);
  }

  ngOnDestroy() {
  }

  getAll(page): void {
    this.fileService.getFiles(this.parentid, this.keyword, page);
  }

  remove(id, index) {
    this.snackBar.open('Processing ...', '', { duration: 900 });
    this.fileService.delete(id).then((res) => {
      this.fileService.setDebouncedQuery(this.parentid, this.keyword, 0);
    });
  }

  selectFile(id, index) {
    this.selectedFile = this.files[index];
    this.onSelect.emit(this.selectedFile);
  }


  @HostListener('scroll', ['$event'])
  onScroll(event) {
    this.elementProp = {
      clientHeight: event.srcElement.clientHeight,
      offsetHeight: event.srcElement.offsetHeight,
      scrollHeight: event.srcElement.scrollHeight,
      scrollTop: event.srcElement.scrollTop,
      scrollTopHistory: this.elementProp.scrollTop,
      threshold: 500
    }
    console.log(this.elementProp);
    // load if there is left then threshold and scroll dirrection is right
    if (this.elementProp.scrollHeight < (this.elementProp.scrollTop + this.elementProp.offsetHeight + this.elementProp.threshold) && this.elementProp.scrollTop > this.elementProp.scrollTopHistory) {
      this.fileService.setDebouncedQuery(this.parentid, this.keyword, this.fileService.getQuery().pagination.page + 1);
      this.elementProp.showLoader = true;
    }
  }

  preview(index) {
    // console.log(this.uploadedFilesParentDom)
    // console.log(this.uploadedFilesParentDom.nativeElement.children[index].children[0]);
    // console.log(window);
    // console.log(document.getElementById('uploaded-files').children[index].children[0]);
    let config: MdDialogConfig = {
      backdropClass: 'file-preview-backdrop-class',
      width: '100vw',
      height: '100vh',
      data: {
        file: this.files[index], fileUrl: this.fileUrl
      }
    };
    let dialogRef = this.dialog.open(FilePreviewComponent, config);
  }

  inputChange(event): void {
    //push files from form to uloading list
    this.fileService.clearUploadData();
    let files = event.target.files;
    for (var _i = 0; _i < files.length; _i++) {
      this.fileService.uploadData(files[_i], _i, this.parentid, this.keyword);
    }
  }
}


