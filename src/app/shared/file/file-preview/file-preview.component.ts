import { Component, Inject, EventEmitter, Output } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { FileService } from './../file.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-file-preview',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.css']
})
export class FilePreviewComponent {
  public fileUrl: string;
  public file: any;

  @Output() onClose = new EventEmitter<any>();

  constructor(public dialogRef: MdDialogRef<FilePreviewComponent>,
    @Inject(MD_DIALOG_DATA) public data: any,
    public sanitizer: DomSanitizer,
    private fileService: FileService) {
    this.fileUrl = data.fileUrl;
    this.file = data.file;
  }

  remove() {
    this.fileService.delete(this.file._id).then((res) => {
      this.fileService.getFiles(this.file.parentid, this.file.keyword, 0);
      this.closeModal();
    });
  }

  closeModal() {
    this.dialogRef.close();
  }
}


