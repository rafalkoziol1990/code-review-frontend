import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule, PreloadAllModules } from '@angular/router';

import { MdlModule } from '@angular-mdl/core';
import { MaterialModule } from '@angular/material';

import { HeaderComponent } from './header.component';

@NgModule({
  entryComponents: [],
  imports: [
    CommonModule,
    MdlModule,
    MaterialModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent
  ],
  providers: [],
  exports: [HeaderComponent]
})
export class HeaderModule { }
