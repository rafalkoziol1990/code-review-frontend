import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MdlModule } from '@angular-mdl/core';
import { MaterialModule, OverlayModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import 'hammerjs';

import { IndexComponent } from './index/index.component';

import { AppRoutingModule } from './app-routing.module';

import { UsersModule } from './users/users.module';
import { AuthGuard } from './users/auth.guard';

import { FileModule } from './shared/file/file.module';
// import { ProjectModule } from './project/project.module';

import { DynamicComponent } from './shared/dynamic-component/dynamic-component';
import { HeaderModule } from './shared/header/header.module';
import { NavModule } from './shared/nav/nav.module';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    MdlModule,
    OverlayModule,
    BrowserAnimationsModule,
    UsersModule,
    FileModule,
    NavModule,
    HeaderModule,
    // ProjectModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    IndexComponent,
    DynamicComponent,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
